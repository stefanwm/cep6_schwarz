/*  
     @author: Mir Farshid Baha
     Contact: farshid.baha@yahoo.com
 */

#include <stm32f4xx.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_spi.h>
#include <stdint.h>
#include "CE_Lib.h"
#include "tft.h"
#include "flash.h"

void read(enum Chip type,uint32_t address, size_t nob, volatile uint8_t data[]){

    int i;
    #ifdef DEBUG
        printf("Read Start\n");
    #endif
    if(address <= MAX_ADDRESS){
		assert(type);
        //sending Op code for READ 
        #ifdef DEBUG
            printf("Reading at address:%x\n", address);
        #endif
        spi_write_byte(READ_FAST) ;
        //sending the starting address
        send_add(address);   
        //2xDummy transmission to get started
        spi_write_byte(DUMMY); 
        spi_write_byte(DUMMY);
        //The actual reading starts here..
        for(i=0;i < nob;i++){
            SPI3->DR = DUMMY;            
            while(!(SPI3->SR & SPI_SR_RXNE));
            data[i] = SPI3->DR;            
        }
        deassert();
        #ifdef DEBUG
            printf("Read End\n");
        #endif
    }
}

//erases 4KB, 32KB, 64KB or the entire chip depending on the OP Code  
void erase_xKB(enum Chip type, uint32_t address, uint8_t op_code){
    #ifdef DEBUG
        printf("Erase start\n");
    #endif
	write_enable(type);  
	unprotect_sector(type, address);

    
	write_enable(type);
	assert(type);
    
    #ifdef DEBUG
    printf("Erasing 4KB at address: %x\n", address);
    #endif   
    spi_write_byte(op_code);
    send_add(address);

	deassert();
    wait_unitl_rdy(type);
    #ifdef DEBUG
       printf("Erase end\n");
    #endif
}

//utility function for erase
void wait_unitl_rdy(enum Chip type){
	uint8_t result = 0;
    #ifdef DEBUG
        printf("Wait until ready\n");
    #endif
	assert(type);
    
	spi_write_byte(READ_STATUS_REG);
	do{
        result = spi_write_byte(DUMMY);
        #ifdef DEBUG
            printf("READY = %x\n", result & 0x01);
        #endif
	}while(result & 0x01);
    #ifdef DEBUG
        printf("READY = %x\n", result & 0x01);
    #endif
	deassert();
}

void wait_write_enable(enum Chip type){
    uint8_t result = 0;
    #ifdef DEBUG
        printf("Wait write enable\n");
    #endif
    spi_write_byte(READ_STATUS_REG);
    do{
        result = spi_write_byte(DUMMY);
        #ifdef DEBUG
            printf("WELI = %x\n", result & 0x02);
        #endif
    }while((result & 0x02));
    #ifdef DEBUG
        printf("WELO = %x\n", result & 0x02);
    #endif
}

void wait_unprotect_sector(enum Chip type){
    uint8_t result = 0;
    #ifdef DEBUG
    printf("Wait unprotect sector\n");
    #endif
    spi_write_byte(READ_STATUS_REG);
    #ifdef DEBUG
            printf("SPRLI = %x\n", spi_write_byte(DUMMY) & 0x80);
    #endif
    do{
        result = spi_write_byte(DUMMY);
    }while(result & 0x80);
    #ifdef DEBUG
        printf("SPRLO = %x\n", result & 0x80);
    #endif

}
//direct programming of the chip
void program_interval(enum Chip type, uint32_t address, size_t length, uint8_t* array){
    int i;
    uint32_t currentIndex;
    currentIndex =0;
/*
 * 
 */
    #ifdef DEBUG
        printf("Program start\n");
    #endif
    while(length){
		write_enable(type);
		unprotect_sector(type, address);
        
		write_enable(type);	
        //sending the programming code and the address to start writing..
		assert(type);
        #ifdef DEBUG
            printf("programing adress: %x\n", address);
        #endif
        spi_write_byte(BYTE_PAGE_PROGRAM);
        send_add(address);
        for(i = 0; (i < PAGE_SIZE)&& (length!= 0);i++){          
			//printf("%d\n",array[currentIndex + i]);
            spi_write_byte(array[currentIndex]);
			currentIndex++;
            length--;
        }
        //unplugging
		deassert();	
        //waiting for the procedure to finish
		wait_unitl_rdy(type);
        address = (address | _256B_MASK) + 1;
    }
    #ifdef DEBUG
        printf("Program end\n");
    #endif    
}

//utility functions
uint8_t spi_write_byte(uint8_t data) {
    SPI3->DR = data;                    
    while(!(SPI3->SR & SPI_SR_RXNE));   // wait until valid data is in rx buffer (RM0090 Chap 28.3.7)
    return SPI3->DR;                    // read data register (RM0090 Chap 28.3.7)
}

void write_enable(enum Chip type){
    assert(type);
    spi_write_byte(WRITE_ENABLE);
    wait_write_enable(type);
	deassert();
}

void unprotect_sector(enum Chip type, uint32_t address){
    assert(type);
    spi_write_byte(UNPROTECT_SECTOR);
    send_add(address);
    wait_unprotect_sector(type);
	deassert();
    
}

//chip select
void assert(enum Chip type){
     if(type == WORK){
         GPIOB->BSRRH = GPIO_Pin_9;
     }else{
         GPIOG->BSRRH = GPIO_Pin_6;
     }
#ifdef DEBUG
    printf("CS = 0\n");
#endif
}

//release chip
void deassert(void){
    GPIOB->BSRRL = GPIO_Pin_9;
    GPIOG->BSRRL = GPIO_Pin_6;
#ifdef DEBUG
    printf("CS = 1\n");
#endif
}


//transfers the address to the chip
uint8_t send_add(uint32_t address){
    uint8_t receiveByte;
    receiveByte = spi_write_byte(address>>16) ; //MSB->LSB,sending 1st address byte      
    receiveByte = spi_write_byte(address>>8);//MSB->LSB,sending 2nd address byte 
    receiveByte = spi_write_byte(address);//MSB->LSB,sending 3rd address byte 
    return receiveByte;
}

//reads manufacturer info
uint32_t manufacturer_info(enum Chip type){
    uint32_t dev_info;
	assert(type);
#ifdef DEBUG
    printf("reading manufacturer id\n");
#endif
    spi_write_byte(READ_DEV_ID);
    dev_info =(
        spi_write_byte(DUMMY)         |
        (spi_write_byte(DUMMY) << 8)  
    );
	deassert();   
    return dev_info;   
}
