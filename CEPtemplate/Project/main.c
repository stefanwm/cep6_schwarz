/*  
     @author: Mir Farshid Baha
     Contact: farshid.baha@yahoo.com
 */

#include <stdint.h>
#include <stm32f4xx.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_gpio.h>
#include "CE_Lib.h"
#include "tft.h"
#include "flash.h"

#define SYS_FREQ     168000000                             
#define TIMER_FREQ       44000                             
#define OFFSET 			  2048         

#define SINE_440HZ_2_PERIODS 400
#define SINE_4400HZ_2_PERIODS 40

#define   buffer_BUFF_SIZE   ( 1 << 2 )                    // buffer_BUFF_SIZE has to equal 2^n 
#define   buffer_EMPTY 0
#define   Q9_22SHIFT 22
#define   Q16_47SHIFT 47                                   //highest resolution because of TIMER_FREQ is highest value in calculation
#define   BACK_TO_Q9_22 25								   // 47-22=25

#define   SIG_TAB_NOE 360
#define   NUMBER_OF_ELEMENTS (SIG_TAB_NOE<<Q9_22SHIFT)

#define   INCREMENT_FOR_440  (uint32_t)(((360ULL<<Q16_47SHIFT)/((44000ULL<<Q16_47SHIFT)/(440ULL<<Q16_47SHIFT))) >> BACK_TO_Q9_22)
#define   INCREMENT_FOR_4400 (uint32_t)(((360ULL<<Q16_47SHIFT)/((44000ULL<<Q16_47SHIFT)/(4400ULL<<Q16_47SHIFT))) >> BACK_TO_Q9_22)
// config spi mode
#define SPI_MODE_3          (SPI_CPOL_High | SPI_CPHA_2Edge)    // set spi mode 3 for AT25DF641 ()
#define SPI_CLK_DIV_4       SPI_BaudRatePrescaler_4  
#define SQUARE_FACTOR   ((4095ULL  << 8 / 16769025ULL << 8 ))

//SINE TABLE
static const int32_t sineSquareTable[SIG_TAB_NOE] =
{
0,	10,	40,	90,	160,	250,	360,	489,	638,	806,
	994,	1200,	1424,	1667,	1928,	2207,	2503,	2816,	3146,	3492,
	3854,	4231,	4623,	5029,	5449,	5883,	6329,	6788,	7259,	7741,
	8233,	8736,	9247,	9768,	10296,	10832,	11375,	11924,	12479,	13038,
	13601,	14168,	14738,	15309,	15882,	16455,	17029,	17601,	18172,	18741,
	19307,	19869,	20427,	20981,	21528,	22069,	22603,	23130,	23648,	24158,
	24658,	25148,	25627,	26094,	26550,	26994,	27424,	27841,	28244,	28632,
	29005,	29363,	29705,	30031,	30340,	30631,	30906,	31162,	31401,	31621,
	31822,	32005,	32168,	32312,	32436,	32541,	32626,	32691,	32736,	32761,
	32766,	32751,	32716,	32661,	32586,	32491,	32376,	32242,	32089,	31916,
	31724,	31513,	31284,	31036,	30771,	30488,	30187,	29870,	29536,	29186,
	28821,	28440,	28044,	27634,	27211,	26774,	26324,	25862,	25389,	24904,
	24409,	23904,	23390,	22868,	22337,	21799,	21255,	20705,	20149,	19589,
	19024,	18457,	17887,	17315,	16742,	16168,	15595,	15023,	14452,	13884,
	13319,	12758,	12201,	11649,	11103,	10564,	10031,	9506,	8990,	8483,
	7986,	7499,	7022,	6557,	6105,	5664,	5237,	4824,	4425,	4040,
	3671,	3317,	2979,	2657,	2353,	2065,	1795,	1543,	1310,	1094,
	898,	720,	561,	422,	303,	203,	123,	63,	23,	3,
	3,	23,	63,	123,	203,	303,	422,	561,	720,	898,
	1094,	1310,	1543,	1795,	2065,	2353,	2657,	2979,	3317,	3671,
	4040,	4425,	4824,	5237,	5664,	6105,	6557,	7022,	7499,	7986,
	8483,	8990,	9506,	10031,	10564,	11103,	11649,	12201,	12758,	13319,
	13884,	14452,	15023,	15595,	16168,	16742,	17315,	17887,	18457,	19024,
	19589,	20149,	20705,	21255,	21799,	22337,	22868,	23390,	23904,	24409,
	24904,	25389,	25862,	26324,	26774,	27211,	27634,	28044,	28440,	28821,
	29186,	29536,	29870,	30187,	30488,	30771,	31036,	31284,	31513,	31724,
	31916,	32089,	32242,	32376,	32491,	32586,	32661,	32716,	32751,	32766,
	32761,	32736,	32691,	32626,	32541,	32436,	32312,	32168,	32005,	31822,
	31621,	31401,	31162,	30906,	30631,	30340,	30031,	29705,	29363,	29005,
	28632,	28244,	27841,	27424,	26994,	26550,	26094,	25627,	25148,	24658,
	24158,	23648,	23130,	22603,	22069,	21528,	20981,	20427,	19869,	19307,
	18741,	18172,	17601,	17029,	16455,	15882,	15309,	14738,	14168,	13601,
	13038,	12479,	11924,	11375,	10832,	10296,	9768,	9247,	8736,	8233,
	7741,	7259,	6788,	6329,	5883,	5449,	5029,	4623,	4231,	3854,
	3492,	3146,	2816,	2503,	2207,	1928,	1667,	1424,	1200,	994,
	806,	638,	489,	360,	250,	160,	90,	40,	10,	0
};

static const int32_t sineTable[SIG_TAB_NOE] = 
{
0,	573,	1147,	1720,	2292,	2864,	3435,	4004,	4573,	5140,
	5706,	6269,	6831,	7391,	7949,	8504,	9056,	9606,	10153,	10697,
	11237,	11774,	12307,	12837,	13362,	13884,	14401,	14914,	15423,	15926,
	16425,	16919,	17407,	17890,	18368,	18840,	19306,	19767,	20221,	20669,
	21111,	21546,	21975,	22397,	22812,	23220,	23621,	24015,	24402,	24781,
	25152,	25516,	25872,	26220,	26560,	26891,	27215,	27530,	27837,	28135,
	28425,	28706,	28978,	29241,	29495,	29741,	29977,	30204,	30421,	30630,
	30829,	31018,	31199,	31369,	31530,	31681,	31823,	31955,	32077,	32189,
	32291,	32384,	32466,	32539,	32601,	32654,	32696,	32729,	32752,	32764,
	32767,	32759,	32742,	32714,	32676,	32629,	32571,	32504,	32426,	32339,
	32241,	32134,	32017,	31890,	31753,	31607,	31451,	31285,	31110,	30925,
	30731,	30527,	30314,	30091,	29860,	29619,	29369,	29111,	28843,	28566,
	28281,	27987,	27685,	27374,	27054,	26726,	26391,	26047,	25695,	25335,
	24967,	24592,	24209,	23819,	23422,	23017,	22606,	22187,	21762,	21329,
	20891,	20446,	19995,	19537,	19074,	18605,	18130,	17649,	17163,	16672,
	16176,	15675,	15169,	14658,	14143,	13624,	13100,	12572,	12041,	11506,
	10967,	10425,	9880,	9332,	8780,	8227,	7670,	7112,	6551,	5988,
	5423,	4857,	4289,	3720,	3149,	2578,	2006,	1433,	860,	287,
	-287,	-860,	-1433,	-2006,	-2578,	-3149,	-3720,	-4289,	-4857,	-5423,
	-5988,	-6551,	-7112,	-7670,	-8227,	-8780,	-9332,	-9880,	-10425,	-10967,
	-11506,	-12041,	-12572,	-13100,	-13624,	-14143,	-14658,	-15169,	-15675,	-16176,
	-16672,	-17163,	-17649,	-18130,	-18605,	-19074,	-19537,	-19995,	-20446,	-20891,
	-21329,	-21762,	-22187,	-22606,	-23017,	-23422,	-23819,	-24209,	-24592,	-24967,
	-25335,	-25695,	-26047,	-26391,	-26726,	-27054,	-27374,	-27685,	-27987,	-28281,
	-28566,	-28843,	-29111,	-29369,	-29619,	-29860,	-30091,	-30314,	-30527,	-30731,
	-30925,	-31110,	-31285,	-31451,	-31607,	-31753,	-31890,	-32017,	-32134,	-32241,
	-32339,	-32426,	-32504,	-32571,	-32629,	-32676,	-32714,	-32742,	-32759,	-32767,
	-32764,	-32752,	-32729,	-32696,	-32654,	-32601,	-32539,	-32466,	-32384,	-32291,
	-32189,	-32077,	-31955,	-31823,	-31681,	-31530,	-31369,	-31199,	-31018,	-30829,
	-30630,	-30421,	-30204,	-29977,	-29741,	-29495,	-29241,	-28978,	-28706,	-28425,
	-28135,	-27837,	-27530,	-27215,	-26891,	-26560,	-26220,	-25872,	-25516,	-25152,
	-24781,	-24402,	-24015,	-23621,	-23220,	-22812,	-22397,	-21975,	-21546,	-21111,
	-20669,	-20221,	-19767,	-19306,	-18840,	-18368,	-17890,	-17407,	-16919,	-16425,
	-15926,	-15423,	-14914,	-14401,	-13884,	-13362,	-12837,	-12307,	-11774,	-11237,
	-10697,	-10153,	-9606,	-9056,	-8504,	-7949,	-7391,	-6831,	-6269,	-5706,
	-5140,	-4573,	-4004,	-3435,	-2864,	-2292,	-1720,	-1147,	-573,	0
};

static volatile int16_t buffer[ buffer_BUFF_SIZE ] = { 0 }; 
static volatile int16_t buffer2[ buffer_BUFF_SIZE ] = { 0 }; 

static volatile uint16_t bufferElements, bufferRdIndx = 0;                            
static volatile int tableSelection, freqSelection = 1;

volatile uint32_t increment = INCREMENT_FOR_440;

volatile int32_t const* pTable = sineTable;
volatile int32_t const* pTableSquared = sineSquareTable; 

volatile uint32_t adress = 0x0;
volatile uint32_t length = 200;
volatile uint8_t spi_readArray[400];


/*
*    ISR for DAC 
*/
void TIM8_UP_TIM13_IRQHandler(void){	
	//Clear IRQ
    TIM8->SR = ~TIM_SR_UIF;	
	//Consume elements in buffer if elements present 
    if (bufferElements > buffer_EMPTY){    

        DAC->DHR12R1 = buffer2[bufferRdIndx];
        TIM8->CCR3= buffer2[bufferRdIndx];
        
      bufferRdIndx = (bufferRdIndx + 1) % buffer_BUFF_SIZE;
      --bufferElements;     
    }
}

/*
*    ISR for external button interrupts
*/
void EXTI9_5_IRQHandler(void){
	//Waveform toggling
	if(EXTI->PR & 1 << 8){
		if(freqSelection){
					increment = INCREMENT_FOR_440;
					length = SINE_440HZ_2_PERIODS;
					adress = 0x0;
		}else{
					increment = INCREMENT_FOR_4400;
					length = SINE_4400HZ_2_PERIODS;
					adress = 0x000200;
		}
		freqSelection = !freqSelection;
	}	
	//Clear pending register by setting it
	EXTI->PR = (1 << 8);
}

/*
*     MAIN
*/
int main(void){        
    /*
	*  Initialization
    */
    enum Chip type;
    char idString[20];
    char *id;
    int16_t test;
    int16_t index; 
    uint32_t squareValue;
    volatile uint16_t* fsmc = (uint16_t *) 0x60000000;
    int i;
    int arrayIndex;
    
    uint32_t oldFreqSelection;
    uint16_t bufferWrIndx = 0;
    uint32_t bufferNextValue = 0;   

    uint8_t spi_writeArray_440[400];
    uint8_t spi_writeArray_4400[20];
    
    initCEP_Board();    
    
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;            // enable clock for GPIOA (RM0090 Chap 6.3.10)
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;            // enable clock for GPIOB
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;   
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN; 
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOEEN; // enable clock for GPIOC
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOFEN;            // enable clock for GPIOF
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOGEN;            // enable clock for GPIOG
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOHEN;            // enable clock for GPIOH
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOIEN;            // enable clock for GPIOI

    /* SPI */ 
    // Set PortB.9 as output (Chip Select 1)
    GPIOB->MODER |= (GPIO_Mode_OUT << (2*9)); 
    // Set PortG.6 as output (Chip Select 2)
    GPIOG->MODER |= (GPIO_Mode_OUT << (2*6));
    
    /* Set SPI Pins to alternate port function */
    // set IO mode to alternate function (RM0090 Chap 8.3.2)
    GPIOC->MODER |= (GPIO_Mode_AF << (2*10)) | (GPIO_Mode_AF << (2*11)) | (GPIO_Mode_AF << (2*12));
    GPIOC->OSPEEDR |= (GPIO_Fast_Speed << (2*10)) | (GPIO_Fast_Speed << (2*11)) | (GPIO_Fast_Speed << (2*12));
    // set alternate function mode for use with spi (RM0090 Chap 8.3.2)
    GPIOC->AFR[1] |= (GPIO_AF_SPI3 << (4*2)) | (GPIO_AF_SPI3 << (4*3)) | (GPIO_AF_SPI3 << (4*4));
    
    /* Set SPI Configuration */
    // enable clock for SPI (RM0090 Chap 6.3.13)
    RCC->APB1ENR |= RCC_APB1ENR_SPI3EN;
    // set SPI configuration(RM0090 Chap 28.5.1)    
    SPI3->CR1 = (SPI_CR1_SPE | SPI_CR1_MSTR | SPI_CR1_SSM | SPI_CR1_SSI | SPI_CLK_DIV_4 | SPI_MODE_3);  
    
   
    // disable chip selects
    deassert();
    
    /* FSMC Configuration */
    RCC->AHB3ENR |= RCC_AHB3ENR_FSMCEN;
	
	//GPIOD
    GPIOD->MODER |= GPIO_Mode_AF | GPIO_Mode_AF << 2*1 | GPIO_Mode_AF << 2*4 | GPIO_Mode_AF << 2*5 
	| GPIO_Mode_AF << 2*7 | GPIO_Mode_AF << (2*8) | GPIO_Mode_AF << (2*9)
	| GPIO_Mode_AF << (2*10) | GPIO_Mode_AF << (2*14) | GPIO_Mode_AF << (2*15);
	
	GPIOD->OTYPER |= 0x00000000;
	GPIOD->PUPDR |= 0x00000000;
    GPIOD->OSPEEDR |= GPIO_Fast_Speed | GPIO_Fast_Speed << 2*1 | GPIO_Fast_Speed << 2*4 | GPIO_Fast_Speed << 2*5 
	| GPIO_Fast_Speed << 2*7 | GPIO_Fast_Speed << (2*8) | GPIO_Fast_Speed << (2*9)
	| GPIO_Fast_Speed << (2*10) | GPIO_Fast_Speed << (2*14) | GPIO_Fast_Speed << (2*15);
	
    GPIOD->AFR[0] |= (GPIO_AF_FSMC) |(GPIO_AF_FSMC << (4*1)) |(GPIO_AF_FSMC << (4*4)) | (GPIO_AF_FSMC << (4*5)) | (GPIO_AF_FSMC << (4*7));    
	GPIOD->AFR[1] |= (GPIO_AF_FSMC) | (GPIO_AF_FSMC << (4*1))  | (GPIO_AF_FSMC << (4*2))  | (GPIO_AF_FSMC << (4*6))  | (GPIO_AF_FSMC << (4*7));
	
	//GPIOE
	GPIOE->MODER |= GPIO_Mode_AF << 2*7 | GPIO_Mode_AF << (2*8) | GPIO_Mode_AF << (2*9)
	| GPIO_Mode_AF << (2*10) | GPIO_Mode_AF << (2*11) | GPIO_Mode_AF << (2*12) | GPIO_Mode_AF << (2*13) | GPIO_Mode_AF << (2*14) | GPIO_Mode_AF << (2*15);
	
	GPIOE->OTYPER |= 0x00000000;
	GPIOE->PUPDR |= 0x00000000;
	GPIOE->OSPEEDR |= GPIO_Fast_Speed << 2*7 | GPIO_Fast_Speed << (2*8) | GPIO_Fast_Speed << (2*9)
	| GPIO_Fast_Speed<< (2*10) | GPIO_Fast_Speed << (2*11) | GPIO_Fast_Speed << (2*12) | GPIO_Fast_Speed << (2*13) | GPIO_Fast_Speed << (2*14) | GPIO_Fast_Speed << (2*15);
	
    GPIOE->AFR[0] |= (GPIO_AF_FSMC << (4*7));    
	GPIOE->AFR[1] |= (GPIO_AF_FSMC) | (GPIO_AF_FSMC << (4*1))  | (GPIO_AF_FSMC << (4*2)) | (GPIO_AF_FSMC << (4*3)) |  (GPIO_AF_FSMC << (4*4))
	| (GPIO_AF_FSMC << (4*5)) | (GPIO_AF_FSMC << (4*6))  | (GPIO_AF_FSMC << (4*7));
	
	FSMC_Bank1->BTCR[0] |=  (FSMC_BCR1_WREN |FSMC_BCR1_MTYP_0 | FSMC_BCR1_MBKEN);
    FSMC_Bank1->BTCR[1] |=  (FSMC_BTR1_ADDSET_0 |FSMC_BTR1_DATAST_2 | FSMC_BTR1_BUSTURN_0);
  
    /*PWM*/
    GPIOB->MODER &=~ (GPIO_Mode_AN << (1 * 2));
    GPIOB->MODER |= GPIO_Mode_AF << (1 * 2);
    GPIOB->OSPEEDR &= ~(GPIO_Mode_AN << (1 * 2));
    GPIOB->OSPEEDR |= GPIO_Speed_50MHz << (1 * 2);
    GPIOB->OTYPER &= ~(1 << 1);
    GPIOB->OTYPER |= GPIO_OType_PP << 1;
    GPIOB->PUPDR &= ~(3 << (1 * 2));
    GPIOB->PUPDR |= GPIO_PuPd_UP << (1 * 2);
    GPIOB->AFR[0] &= ~(0xf << (1 * 4));
    GPIOB->AFR[0] |= GPIO_AF_TIM8 << (1 * 4);
    
    /*DAC and TIMER8 */
    RCC->APB1ENR |= RCC_APB1ENR_DACEN;                  // enable clock for DAC                 {>RM0090 chap 6.3.13}
    RCC->APB2ENR |= RCC_APB2ENR_TIM8EN;                 // enable clock for timer 8             {>RM0090 chap 6.3.14}
    RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;				// enable SYSCFGEN	
	
    SYSCFG->EXTICR[2] = (SYSCFG->EXTICR[2] & ~0xf) | 5;	// Select port F for Line 8 (PF8)
    EXTI->RTSR |= (1 << 8);
    EXTI->IMR  |= (1 << 8);
	
    NVIC_SetPriority(EXTI9_5_IRQn, 2);
	  NVIC_EnableIRQ(EXTI9_5_IRQn);
	
    TIM8->CR1 = 0;                                          
    TIM8->CR2 = 0;                                         
    TIM8->PSC = 1 - 1;                                          // NO prescaler                     {>RM0090 chap 17.4.11} 
    TIM8->ARR = 4096 - 1;      // 168000000Hz / 44000Hz            {>RM0090 chap 17.4.12}
    TIM8->CCMR2 = TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3M_1;
    TIM8->CCER = TIM_CCER_CC3NE;
    TIM8->BDTR = TIM_BDTR_MOE;
    TIM8->DIER = TIM_DIER_UIE;                              // enable interrupt                 {>RM0090 chap 17.4.4}
    TIM8->CR1 = TIM_CR1_CEN | TIM_CR1_ARPE;					// auto-reload preload enable       {>RM0090 chap 17.4.1}
    
    NVIC_SetPriorityGrouping(5);       						
    NVIC_SetPriority( TIM8_UP_TIM13_IRQn,4);                
    NVIC_EnableIRQ( TIM8_UP_TIM13_IRQn );                   // enable IRQ
    
    GPIOA->MODER = (GPIOA->MODER & ~(11 << (GPIO_PinSource4 * 2))) | (GPIO_Mode_AN << (GPIO_PinSource4 * 2));
    DAC->CR = 0;                                   // ControlRegister: configure / keep defaults     {>RM0090 chap 14.5.1}
    DAC->CR |= DAC_CR_EN1;                         // ControlRegister: Enable DAC channel1           {>RM0090 chap 14.5.1}
    DAC->CR |= DAC_CR_BOFF1;

    /* Select memory unit */
    type = ORIGINAL;
	

    /* Read manufacturer info */ 
    sprintf(idString, "%x", manufacturer_info(type));
    id = idString; 
    TFT_cls();
    TFT_gotoxy(1,1);
    TFT_puts(id);
	test = 0;
	
	for( i = 0; i < 10; i++){
		fsmc[0] = 0xD;
		//while((GPIOE->IDR &  (1 << (1*6))) == 0);
//		test = fsmc[0];
		//printf("Wert = %x", test);
		//fflush(stdout);
	}
        /*
    *   Fill Array with two sine periods and then write to SPI
    */
    //Erase 4KB block before writing
    erase_xKB(type, 0x0, ERASE_4KB);

    //Fill 440Hz sine in Array
    increment = INCREMENT_FOR_440;
    for(i = 0;i < SINE_440HZ_2_PERIODS; i = i+2){
        spi_writeArray_440[i] = OFFSET + (pTable[bufferNextValue>>Q9_22SHIFT] >> 4);
        spi_writeArray_440[i + 1] = (OFFSET + (pTable[bufferNextValue>>Q9_22SHIFT] >> 4)) >> 8;
        bufferNextValue += increment;
        
        if(bufferNextValue > NUMBER_OF_ELEMENTS){
           bufferNextValue = bufferNextValue - NUMBER_OF_ELEMENTS;
        }
    }       
    program_interval(type, 0x0, SINE_440HZ_2_PERIODS, spi_writeArray_440);
    
    //Fill 4400Hz sine in array
    increment = INCREMENT_FOR_4400;
    for(i = 0; i < SINE_4400HZ_2_PERIODS; i = i + 2){
       spi_writeArray_4400[i] = OFFSET + (pTable[bufferNextValue>>Q9_22SHIFT] >> 4);
       spi_writeArray_4400[i + 1] = (OFFSET + (pTable[bufferNextValue>>Q9_22SHIFT] >> 4)) >> 8;
       bufferNextValue += increment;
        
       if(bufferNextValue > NUMBER_OF_ELEMENTS){
           bufferNextValue = bufferNextValue - NUMBER_OF_ELEMENTS;
       }
    }
    program_interval(type, 0x000200, SINE_4400HZ_2_PERIODS, spi_writeArray_4400);
    
    /*
    *   Main
    */
    arrayIndex = 0;
    increment = INCREMENT_FOR_440;
    #ifdef DEBUG
        printf("Filling Array for sine output\n");
    #endif
    while(1){
         if(freqSelection != oldFreqSelection){
            read(type, adress, length, spi_readArray);
            oldFreqSelection = freqSelection;
         }  
         
         if (bufferElements == buffer_EMPTY){   
            do{
                index = bufferNextValue >> Q9_22SHIFT;
                squareValue = pTable[index] * pTable[index];
                squareValue = squareValue >> 19; //To 12-Bit
                
                if(pTable[index] < 0){
                    squareValue = squareValue * -1;               
                }
                buffer2[bufferWrIndx] = OFFSET + squareValue;
                
                bufferNextValue += increment;
                if(bufferNextValue > NUMBER_OF_ELEMENTS){
                    bufferNextValue = bufferNextValue - NUMBER_OF_ELEMENTS;
                }
                
                buffer[bufferWrIndx] = spi_readArray[arrayIndex] | (spi_readArray[arrayIndex + 1] << 8);
                arrayIndex = arrayIndex + 2;   
                if(arrayIndex > length - 1){
                   arrayIndex = 0;
                }
                 
                bufferElements++;
                bufferWrIndx = (bufferWrIndx + 1) % buffer_BUFF_SIZE;
            } while (bufferElements != buffer_BUFF_SIZE);     
        }              
    }
}



